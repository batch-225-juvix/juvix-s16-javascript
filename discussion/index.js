// Assignment Operators
	// Basic Assignment Operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
console.log(assignmentNumber);

	// Addition Assignment Operator

	let totalNumber = assignmentNumber + 2;
	console.log("Result of addition assignment opeartor: " + totalNumber);

	// Arithmetic Operator (+, -, *, /, %)

	let x = 2;
	let y = 5;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = y / x;
	console.log("Result of quotient operator: " + quotient);

// modulo operator
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);





// Comparison Operator

// Equality Operator (==)

/*

	- Checks whether the operands are equal / have the same content.
	- Note: = is for assignment operator.
	- Note: == is for equality operator.
	- Attempts to Convert and Compare operands of different data types.
	- True == 1 and False == 0
*/

let juan = 'juan'; 

console.log(1 == 1); // True
console.log(1 == 2); // False
console.log(1 == '1'); // True
console.log(0 == false); // True
//Compares two strings that are the same 
console.log('juan' == 'juan'); // True
//Compares a string with the variable "juan" declared above 

console.log('juan' == juan); // True



// Inequality operator 

/* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */
			console.log(1 != 1); // False
			console.log(1 != 2); // True
			console.log(1 != '1'); // False
			console.log(0 != false); // False
			console.log('juan' != 'juan'); // False
			console.log('juan' != juan);  // False

/*
		- Checks whether the operands are equal / have the same content.
		- Note: = is for assignment operator
		- Note: == is for equality operator
		- Attempts to Convert and Compare operands of different data types.
		- True == 1 and False == 0
	*/



// Strick Equality Operator (===)

/* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct.
            - Magkaiba ng data Types so it is false.
        */
console.log(1 === 1); // True
console.log(1 === 2); // false
console.log(1 === '1'); // false
console.log(0 === false); // false
console.log('juan' === 'juan'); // true
console.log('juan' === juan); // true


	// Relational Operators
//Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65 // false
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65 // true

        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65 // false
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65 // true

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual)



// TRUTH TABLE //
        // Logical Operators (&& = And   || = Or   ! = Not)

        let isLegalAge = true;
        let isRegistered = false;


        // Logical Operator (&& - Double Ampersand - And Operator)
        // Return true if all operands are true
        // Return true if all operands are true
        // Note True = 1 false = 0

        // 1 && 1 = true
        // 1 && 0 = false
        // 0 && 1 = false
        // 0 && 0 = false

        let allRequrirementsMet = isLegalAge && isRegistered;
        console.log("Result of Logical AND Operator " + allRequrirementsMet);

        // Logical Or Operator (|| - Double Pipe)
	// Returns true if one of the operands are true.
	// Once meron number 1, automatic True agad.

	// 1 || 1 = true
	// 1 || 0 = true
	// 0 || 1 = true
	// 0 || 0 = false

	let someRequirementsMet = isLegalAge || isRegistered; 
	console.log("Result of logical OR Operator: " + someRequirementsMet);


	// Logical Not Operator (! - Exclamation Point)
	// Returns the opposite value 
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " + someRequirementsNotMet); // false


	// legal - true isRegistered - false

	let Requirements = !isLegalAge || !isRegistered;
	console.log(Requirements)

	/*
	tingin ko pwede siya sa mga instances like tama ang usename pero mali ang password, naka not si password, then magreregister as true siya kasama ni username, then nakaAnd sila para magappear yung message na "Password incorrect"*/

