// Mini Activity

/*
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);


	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);

	// Mini Activity:

	// Multiple Operators and Parentheses (MDAS and PEMDAS)


	/*When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

	let mDas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mDas operation: " + mDas);

	/*let mDas2 = 6 / 2 * (1 + 2);*/

				/*
				1. (1 + 2) = 3
				2. 2 * 3 = 6
				3. 



				*/
	console.log("Result of mDas operation: " + mDas);
	/*console.log("Result of mDas2 operation: " + mDas2);*/

let pemDas = 1 + (2 - 3) * (4 / 5);


	/*
	1. 4 / 5 = 0.8
	2. 2 - 3 = -1
	3. 1 + 

	*/
console.log(pemDas);



  /*We can think of it po as an example po on how we apply mathematical principles alongside programming principles. :D Or in other words po, gumagawa lang tayo ng "computations" gamit ang variables (na a-assignan natin ng value later on) :D*/

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
- The operations were done in the following order:*/
/*1. 3 * 4 = 12
2. 12 / 5 = 2.4
3. 1 + 2 = 3
4. 3 - 2.4 = 0.6*/

let pemDas1 = 1 + 2 - (3 * 4) / 5
console.log(pemDas1)

/*2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.*/


/*    let minutesHour = 60;
    let hoursDay = 24;
    let daysWeek = 7;
    let weeksMonth = 4;
    let monthsYear = 12;
    let daysYear = 365;*/



/*let minutesHour1 = 60 * 24 * 7 * 4 * 12 * 365;
console.log(minutesHour1)
*/


// CORRECT ANSWER: 
let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let resultMinutes = (minutesHour * hoursDay) * daysYear;
	console.log("There are " + resultMinutes + " minutes in a year.")


/*    3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.*/

    let tempCelsius = 132;
    let resultFahrenheit = (tempCelsius * 1.8000) + 32;
    console.log(tempCelsius + "degrees Celsius when converted to Farenheit is " + resultFahrenheit);



				/* ™╬--ACIVITY--╬™*/


/*
	4a. Given the values below, identify if the values of the following variable are divisible by 8.
	   -Use a "modulo operator" to identify the divisibility of the number to 8.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
	   -Log a message in the console if num7 is divisible by 8.
	   -Log the value of isDivisibleBy8 in the console.

*/
	/*Example or guides*/
	let x = 9999;
	let y = 8;

	let remainder = y % x;
console.log("variable: " + remainder);





/*modulo operator starts*/	
	let num7 = 165;
	let divisible = 8;

	let isDivisibleBy8 = num7 % divisible;
	//Log the value of the remainder in the console.
	console.log("Is num7 divisible by 8?" + isDivisibleBy8);
	//Log the value of isDivisibleBy8 in the console.
	console.log ('The remainder of 165 divided by 8 is 5');
/*modulo operator ends*/	

/*strict equality operator starts*/
console.log(8 === 8); // True
console.log(8 === 165); // false
console.log(8 === '8'); // false
console.log(0 === false); // false
console.log('165' === '165'); // true
console.log('165' === ('165') ); // true
/*strict equality operator ends*/

/*
	4b. Given the values below, identify if the values of the following variable are divisible by 4.
	   -Use a modulo operator to identify the divisibility of the number to 4.
	   -Save the result of the operation in an appropriately named variable.
	   -Log the value of the remainder in the console.
	   -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
	   -Log a message in the console if num8 is divisible by 4.
	   -Log the value of isDivisibleBy4 in the console.

*/

/*modulo operator starts*/
	let num8 = 348;
	let divisible1 = 4;

	let isDivisibleBy4 = num8 % divisible1;
	//Log the value of the remainder in the console.
	console.log("Is num8 divisible by 4?" + isDivisibleBy4);
	//Log the value of isDivisibleBy4 in the console.
	console.log("The remainder of 348 divided by 4 is 0");
/*strict equality operator ends*/


/*strict equality operator starts*/
console.log(4 === 4); // True
console.log(4 === 348); // false
console.log(4 === '4'); // false
console.log(4 === false); // false
console.log('348' === '348'); // true
console.log('348' === ('348') ); // true
/*strict equality operator ends*/

